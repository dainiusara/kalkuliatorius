﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrafinePrograma1_3_fix
{
    public partial class Form1 : Form
    {
        string x1 = "";
        string operacija = "";


        public Form1()
        {
            InitializeComponent();
        }
        private void RasykIEkrana(string skaicius)
        {
            if (ekranas.Text.Length == 1 && ekranas.Text[0] == '0')
            {
                ekranas.Clear();
            }
                if (ekranas.Text.Length < 8)
            {
                ekranas.Text = ekranas.Text + skaicius;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RasykIEkrana("1");
        }

    private void button2_Click(object sender, EventArgs e)
        {
            RasykIEkrana("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RasykIEkrana("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RasykIEkrana("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RasykIEkrana("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RasykIEkrana("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            RasykIEkrana("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            RasykIEkrana("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            RasykIEkrana("9");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            RasykIEkrana("0");
        }

        private void button11_Click(object sender, EventArgs e) /// zenklas =
        {
            float skaicius1 = Convert.ToSingle(x1);
            float skaicius2 = Convert.ToSingle(ekranas.Text);

            float skaicius3 = 0;
            if (operacija == "+")
            {
                skaicius3 = skaicius1 + skaicius2;
                ekranas.Text = skaicius3.ToString();
            }
            if (operacija == "-")
            {
                skaicius3 = skaicius1 - skaicius2;
                ekranas.Text = skaicius3.ToString();
            }
            if (operacija == "/")
            {
                skaicius3 = skaicius1 / skaicius2;
                ekranas.Text = skaicius3.ToString();
            }
            if (operacija == "*")
            {
                skaicius3 = skaicius1 * skaicius2;
                ekranas.Text = skaicius3.ToString();
            }
        }

        private void button12_Click(object sender, EventArgs e)/// zenklas /
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "/";
        }

        private void button13_Click(object sender, EventArgs e)/// zenklas *
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "*";
        }

        private void button14_Click(object sender, EventArgs e)/// zenklas -
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "-";
        }

        private void button15_Click(object sender, EventArgs e)/// zenklas +
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "+";
        }

        private void button16_Click(object sender, EventArgs e)/// zenklas .
        {
            if (ekranas.Text.Contains(".")) 
            {
                return;
            }
            ekranas.Text = ekranas.Text + ".";
        }

        private void button17_Click(object sender, EventArgs e)/// zenklas C
        {
            ekranas.Clear();
        }

        private void button18_Click(object sender, EventArgs e)/// zenklas +-
        {
            x1 = ekranas.Text;
            operacija = "+-";
            if (operacija == "+-")
            {
                float skaicius1 = Convert.ToSingle(x1);
                float skaicius2 = Convert.ToSingle(ekranas.Text);
                skaicius1 = skaicius1 * -1;
                ekranas.Text = skaicius1.ToString();
            }
        }

        private void button19_Click(object sender, EventArgs e)/// zenklas trinti
        {
            int skaicius1 = ekranas.Text.Length;
            ekranas.Text = ekranas.Text.Substring(0, skaicius1 - 1);
        }
    }
}